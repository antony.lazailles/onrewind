import React, { Component } from 'react'

//composant event dans lequel on indique la manière dont on voudra afficher notre évènement
class Event extends Component {
    render(){
        return (
        <div>
            <h1>
                <div> <img src={this.props.video} style={{ height: 100, width: 100 }} /></div>
                <h1>{this.props.name}</h1>
                <h1>{this.props.challenger}</h1>
                <h1>{this.props.tags}</h1>
            </h1>
        </div>
        );
    }
}

export default Event;