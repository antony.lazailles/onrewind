import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { render } from 'react-dom';
import { ApolloProvider, useQuery } from '@apollo/react-hooks';
import Event from './event.js';
import './index.css';

//constante qui stocke la connexion à l'api
const client = new ApolloClient({
    uri: 'https://staging-graphql-service.onrewind.tv/',
    headers:{"x-account-key": "ryHvne_jFV"},
});

//constante qui stocke notre requête
const GET_EVENTS = gql`
{
    allEvents(tags:"vod", limit:10){
        items{id, name, Video{poster}, Challengers{name, pictureUrl}, Tags{name}, Streams{name, url}
        }
      }  
}`;

//on initialise la variable dataAll qui stockera nos données
var dataAll = "";

//component data qui met à jour le contenu de la variable dataAll
function Data() {
  const [loading, error, data] = useQuery(GET_EVENTS);
  if (loading) return <p>Loading ...</p>;
  if (error) return <p>Error</p>;

  dataAll = data.allEvents.items;
}

//component dans lequel je veux circuler dans mon dataAll pour afficher chaque évènement
class Index extends React.Component{
  render(){
      return(
          <ApolloProvider client={client}>
               <div>
                 <Data/>
                  {this.state.dataAll.map((row) => (
                    <Event name={row.name} video={row.Video.poster} challengers={row.Challengers} tags={row.Tags}/>
      
                  ))}
                </div>
          </ApolloProvider>
      )
  }
}

ReactDOM.render(<Index />, document.getElementById('app'))